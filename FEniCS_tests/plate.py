from fenics import*
import matplotlib.pyplot as plt
import numpy as np


def plate_solver():
    # --------------------
    # Parameters
    # --------------------
    E = 10920.0  # Youngs modulus
    nu = 0.3  # Poissons ratio
    k = 5.0 / 6.0  # Shear correction factor
    t = 0.0001  # Thickness

    # --------------------
    # Define geometry
    # --------------------
    mesh = UnitSquareMesh(32, 32)

    # --------------------
    # Define spaces
    # --------------------
    element = MixedElement([VectorElement('P', triangle, 3),
                            FiniteElement('P', triangle, 3)])
    V = FunctionSpace(mesh, element)

    theta, w = TrialFunctions(V)
    d_theta, d_w = TestFunctions(V)

    # --------------------
    # Boundary conditions
    # --------------------
    bc_u = Constant(0.0)

    def whole_boundary(x, on_boundary):
        return on_boundary

    bc = [DirichletBC(V, Constant((0.0, 0.0, 0.0)), whole_boundary)]

    # --------------------
    # Initialization
    # --------------------
    X = Function(V)
    f = Constant(1.0)

    # --------------------
    # Solution
    # --------------------
    kappa = sym(grad(theta))
    d_kappa = sym(grad(d_theta))
    D = (E*t**3)/(12.0*(1.0-nu**2))
    D2 = (E*k*t)/(2.0*(1.0+nu))
    A = inner(D*((1.0-nu)*div(theta)*Identity(2) + nu*kappa), d_kappa)*dx
    A += inner(D2*grad(w), grad(d_w))*dx + inner(D2*theta, grad(d_w))*dx
    A += inner(D2*(grad(w) + theta), d_theta)*dx
    L = inner(f*t**3, d_w)*dx

    solve(A == L, X, bc)
    theta_, w_ = X.split(deepcopy=True)

    print(np.amax(w_.vector().get_local()))

    plot(w_)
    plt.show()

    file = File("plate/plate01.pvd")
    file << w_

from fenics import*
import matplotlib.pyplot as plt


def beam_solver(h, nbas):
    # --------------------
    # Parameters
    # --------------------
    E = 50.0e9
    G = 25.0e9
    b = 0.1
    # h = 0.01
    Ar = b*h
    I = (1.0/12.0)*b*h*h*h

    k = 1
    f_lin = 1.0
    n = 100
    l = 2.0
    F = 1.0

    # --------------------
    # Define geometry
    # --------------------
    mesh = IntervalMesh(n, 0, l)

    # --------------------
    # Define spaces
    # --------------------
    P1 = FiniteElement('P', interval, nbas)
    # Function Space P2 for "penalty term"
    P2 = FiniteElement('DG', interval, 0)
    element = MixedElement([P1, P1, P1, P2])
    V = FunctionSpace(mesh, element)
    # W = FunctionSpace(mesh, 'P', 2)

    d_u, d_w, d_phi, d_lam = TestFunctions(V)
    u, w, phi, gam = TrialFunctions(V)

    # --------------------
    # Boundary conditions
    # --------------------
    def left_end(x):
        return near(x[0], 0.0)

    def right_end(x):
        return near(x[0], 2.0)

    bc1 = DirichletBC(V.sub(0), Constant(0.0), left_end)
    bc2 = DirichletBC(V.sub(1), Constant(0.0), left_end)
    bc3 = DirichletBC(V.sub(1), Constant(0.0), right_end)
    bc4 = DirichletBC(V.sub(0), Constant(0.0), right_end)
    bc = [bc1, bc2, bc3, bc4]

    # --------------------
    # Initialization
    # --------------------
    X = Function(V)

    # --------------------
    # Solution
    # --------------------
    # Weak form with shear-locking penalty term
    A = d_u.dx(0)*E*Ar*u.dx(0)*dx + \
        d_w.dx(0)*G*Ar*gam*dx + \
        d_phi.dx(0)*E*I*phi.dx(0)*dx + d_phi*G*Ar*gam*dx + \
        d_lam*(gam - w.dx(0) - phi)*dx + \
        Constant(0.0)*d_w*dx

    A_ass, b_ass = assemble_system(lhs(A), rhs(A), bc)

    point_force = PointSource(V.sub(1), Point(1.0), 1)
    point_force.apply(b_ass)

    solve(A_ass, X.vector(), b_ass)

    u_, w_, phi_, gam_ = X.split(deepcopy=True)

    return w_

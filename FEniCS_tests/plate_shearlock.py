from fenics import*
import matplotlib.pyplot as plt
import numpy as np


def plate_solver():
    # --------------------
    # Parameters
    # --------------------
    E = 10920.0  # Youngs modulus
    nu = 0.3  # Poissons ratio
    k = 5.0 / 6.0  # Shear correction factor
    t = 0.001  # Thickness

    # --------------------
    # Define geometry
    # --------------------
    mesh = UnitSquareMesh(32, 32)

    # --------------------
    # Define spaces
    # --------------------
    element = MixedElement([VectorElement('P', triangle, 1),
                            FiniteElement('P', triangle, 1),
                            VectorElement('P', triangle, 1)])
    V = FunctionSpace(mesh, element)

    theta, w, gam = TrialFunctions(V)
    d_theta, d_w, d_gam = TestFunctions(V)

    # --------------------
    # Boundary conditions
    # --------------------
    def whole_boundary(x, on_boundary):
        return on_boundary

    bc1 = DirichletBC(V.sub(0), Constant((0.0, 0.0)), whole_boundary)
    bc2 = DirichletBC(V.sub(1), Constant(0.0), whole_boundary)
    bc = [bc1, bc2]

    # --------------------
    # Initialization
    # --------------------
    X = Function(V)
    f = Constant(1.0)

    # --------------------
    # Weak form
    # --------------------
    kappa = sym(grad(theta))
    d_kappa = sym(grad(d_theta))
    D = (E*t**3)/(12.0*(1.0-nu**2))
    D2 = (E*k*t)/(2.0*(1.0+nu))

    # Bending mode
    # A = inner(D*((1.0-nu)*div(theta)*Identity(2) + nu*kappa), d_kappa)*dx
    A = D*((1.0-nu)*inner(kappa, d_kappa) + nu*div(theta)*div(d_theta))*dx
    # Shear mode
    A += inner(D2*gam, grad(d_w))*dx + inner(D2*gam, d_theta)*dx
    # Penalty term for shear-locking prevention
    A += inner(D2*(gam - grad(w) - theta), d_gam)*dx

    L = inner(f*t**3, d_w)*dx

    # --------------------
    # Solution
    # --------------------
    solve(A == L, X, bc)
    theta_, w_, gam_ = X.split(deepcopy=True)

    print(np.amax(w_.vector().get_local()))

    plot(w_)
    plt.show()

    file = File("plate/plate01.pvd")
    file << w_

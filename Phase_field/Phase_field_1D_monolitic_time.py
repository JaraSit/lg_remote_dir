# -------------------------
# Phase_field_1D_monolitic_time.py
# -------------------------

# -------------------------
# Description:
# - 1D phase field static, monolitic solver with time evolution
#
# Last edit: 04.04. 2019
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt
import numpy as np


# --------------------
# Functions and classes
# --------------------
def left_end(x):
	return fe.near(x[0], 0.0)


def right_end(x):
	return fe.near(x[0], l)


def middle_point(x):
	return fe.near(x[0], l / 2)


# --------------------
# Parameters
# --------------------
n = 200
l = 1.0
A = 1.0
lc = 0.01
Gc = 1.0

# Time parameters
t = 0.1
t_end = 5.0
dt = 0.1

# Iteration parameters
iterr = 1
error = 1.0
toll = 1.0e-4
maxiter = 30

# --------------------
# Define geometry
# --------------------
mesh = fe.IntervalMesh(n, 0.0, l)

fe.plot(mesh, "Mesh")
plt.show()

# --------------------
# Define spaces
# --------------------
V = fe.FunctionSpace(mesh, "CG", 1)
W = fe.FunctionSpace(mesh, "DG", 0)
P1 = fe.FiniteElement('P', fe.interval, 1)
element = fe.MixedElement([P1, P1])
M = fe.FunctionSpace(mesh, element)

# --------------------
# Boundary conditions
# --------------------
u_D = fe.Expression("t", t=0.0, degree=0)
BC_u_1 = fe.DirichletBC(M.sub(0), fe.Constant(0.0), left_end)
BC_u_2 = fe.DirichletBC(M.sub(0), u_D, right_end)
BC_u = [BC_u_1, BC_u_2]

# --------------------
# Initialization
# --------------------
X = fe.Function(M)
u, d = fe.split(X)

lower = fe.Function(M)
upper = fe.Function(M)
ninfty = fe.Function(V); ninfty.vector()[:] = -np.infty
pinfty = fe.Function(V); pinfty.vector()[:] = np.infty

fa = fe.FunctionAssigner(M, [V, V])
fa.assign(lower, [ninfty, fe.interpolate(fe.Constant(0.0), V)])
fa.assign(upper, [pinfty, fe.interpolate(fe.Constant(1.0), V)])

E = fe.Expression("1.0 - 0.2*near(x[0], 0.5, 0.001)", degree=1)
fe.plot(fe.project(E, V))
plt.show()

# --------------------
# Variational problem
# --------------------
En = (1-d)**2*E*A*(u.dx(0))**2*fe.dx + Gc*(0.5*d*d/lc + 0.5*lc*(d.dx(0))**2)*fe.dx

F = fe.derivative(En, X, fe.TestFunction(M))
H = fe.derivative(F, X, fe.TrialFunction(M))

snes_solver_parameters = {"nonlinear_solver": "snes",
									"snes_solver": {"linear_solver": "lu",
									"relative_tolerance": 1e-4,
									"maximum_iterations": 20,
									"report": True,
									"error_on_nonconvergence": False}}

problem = fe.NonlinearVariationalProblem(F, X, BC_u, H)
problem.set_bounds(lower, upper)

solver = fe.NonlinearVariationalSolver(problem)
solver.parameters.update(snes_solver_parameters)

# --------------------
# Time loop
# --------------------
force = []
time = []

while t < t_end:

	u_D.t = t

	solver.solve()
	uu, dd = X.split()

	fa.assign(lower, [ninfty, fe.interpolate(dd, V)])

	time.append(t)
	sigma = fe.project((1.0-dd)**2*E*A*uu.dx(0), W)
	force.append(sigma.vector()[n-1])

	t += dt

plt.plot(time, force)
plt.xlabel("t")
plt.ylabel("reaction")
plt.show()

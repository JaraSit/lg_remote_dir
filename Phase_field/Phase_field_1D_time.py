# -------------------------
# Phase_field_1D_time.py
# -------------------------

# -------------------------
# Description:
# - 1D phase field static with time evolution
#
# Last edit: 03.04. 2019
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt


# --------------------
# Functions and classes
# --------------------
def left_end(x):
	return fe.near(x[0], 0.0)


def right_end(x):
	return fe.near(x[0], l)


def middle_point(x):
	return fe.near(x[0], l / 2)


# --------------------
# Parameters
# --------------------
n = 200
l = 1.0
A = 1.0
lc = 0.01
Gc = 1.0

# Time parameters
t = 0.0
t_end = 10.0
dt = 0.1

# Iteration parameters
iterr = 1
error = 1.0
toll = 1.0e-4
maxiter = 30

# --------------------
# Define geometry
# --------------------
mesh = fe.IntervalMesh(n, 0.0, l)

fe.plot(mesh, "Mesh")
plt.show()

# --------------------
# Define spaces
# --------------------
V = fe.FunctionSpace(mesh, "CG", 1)
W = fe.FunctionSpace(mesh, "DG", 0)
u_tr = fe.TrialFunction(V)
u_test = fe.TestFunction(V)
d_tr = fe.TrialFunction(V)
d_test = fe.TestFunction(V)


# --------------------
# Boundary conditions
# --------------------
u_D = fe.Expression("t", t=0.0, degree=0)
BC_u_1 = fe.DirichletBC(V, fe.Constant(0.0), left_end)
BC_u_2 = fe.DirichletBC(V, u_D, right_end)
BC_u = [BC_u_1, BC_u_2]
BC_s = []

# --------------------
# Initialization
# --------------------
u = fe.Function(V)
u_old = fe.Function(V)
d = fe.Function(V)
d_old = fe.Function(V)

E = fe.Expression("1.0 - 0.1*near(x[0], 0.5, 0.001)", degree=1)
fe.plot(fe.project(E, V))
plt.show()

# --------------------
# Variational problem
# --------------------
E_du = (1-d)**2*E*A*fe.inner(fe.grad(u_test), fe.grad(u_tr))*fe.dx
E_ds = -0.5*E*A*fe.inner(fe.grad(u), fe.grad(u))*fe.inner(1.0-d_tr, d_test)*fe.dx + Gc*(1.0/lc*fe.inner(d_tr, d_test) +
		lc*fe.inner(fe.grad(d_tr), fe.grad(d_test)))*fe.dx

# --------------------
# Staggered loop
# --------------------

force = []
time = []

while t < t_end:

	u_D.t = t
	error = 1.0
	iterr = 1
	d.assign(fe.interpolate(fe.Constant(0.0), V))

	while error > toll and iterr < maxiter:
		# Displacement solution
		fe.solve(fe.lhs(E_du) == fe.rhs(E_du), u_old, BC_u)
		error1 = fe.norm(fe.project(u-u_old, V))
		u.assign(u_old)

		# Damage solution
		fe.solve(fe.lhs(E_ds) == fe.rhs(E_ds), d_old, BC_s)
		error2 = fe.norm(fe.project(d-d_old, V))
		d.assign(d_old)

		# While condition
		iterr += 1
		error = max(error1, error2)

		if iterr == maxiter:
			print("max iteration reached")

	time.append(t)
	sigma = fe.project((1.0-d)**2*E*A*u.dx(0), W)
	force.append(sigma.vector()[n-1])

	t += dt

plt.plot(time, force)
plt.xlabel("t")
plt.ylabel("reaction")
plt.figure()
fe.plot(u)
plt.xlabel("x")
plt.ylabel("u(x)")
plt.figure()
fe.plot(d)
plt.xlabel("x")
plt.ylabel("d(x)")
plt.show()

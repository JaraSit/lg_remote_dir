# -------------------------
# Phase_field_1D_monolitic.py
# -------------------------

# -------------------------
# Description:
# - 1D phase field static, monolitic solver
#
# Last edit: 04.04. 2019
# -------------------------

import fenics as fe
import matplotlib.pyplot as plt


# --------------------
# Functions and classes
# --------------------
def left_end(x):
	return fe.near(x[0], 0.0)


def right_end(x):
	return fe.near(x[0], l)


def middle_point(x):
	return fe.near(x[0], l / 2)


# --------------------
# Parameters
# --------------------
n = 200
l = 1.0
A = 1.0
lc = 0.01
Gc = 1.0

# Time parameters
t = 0.0
t_end = 10.0
dt = 0.1

# Iteration parameters
iterr = 1
error = 1.0
toll = 1.0e-4
maxiter = 30

# --------------------
# Define geometry
# --------------------
mesh = fe.IntervalMesh(n, 0.0, l)

fe.plot(mesh, "Mesh")
plt.show()

# --------------------
# Define spaces
# --------------------
V = fe.FunctionSpace(mesh, "CG", 1)
W = fe.FunctionSpace(mesh, "DG", 0)
P1 = fe.FiniteElement('P', fe.interval, 1)
element = fe.MixedElement([P1, P1])
M = fe.FunctionSpace(mesh, element)

# --------------------
# Boundary conditions
# --------------------
u_D = fe.Expression("t", t=0.0, degree=0)
BC_u_1 = fe.DirichletBC(M.sub(0), fe.Constant(0.0), left_end)
BC_u_2 = fe.DirichletBC(M.sub(0), u_D, right_end)
BC_u = [BC_u_1, BC_u_2]

# --------------------
# Initialization
# --------------------
X = fe.Function(M)
u, d = fe.split(X)

E = fe.Expression("1.0 - 0.1*near(x[0], 0.5, 0.001)", degree=1)
fe.plot(fe.project(E, V))
plt.show()

# --------------------
# Variational problem
# --------------------
En = (1-d)**2*E*A*(u.dx(0))**2*fe.dx + Gc*(0.5*d*d/lc + 0.5*lc*(d.dx(0))**2)*fe.dx

# --------------------
# Staggered loop
# --------------------

u_D.t = 3.0

F = fe.derivative(En, X, fe.TestFunction(M))
H = fe.derivative(F, X, fe.TrialFunction(M))

snes_solver_parameters = {"nonlinear_solver": "snes",
									"snes_solver": {"linear_solver": "lu",
									"relative_tolerance": 1e-4,
									"maximum_iterations": 20,
									"report": True,
									"error_on_nonconvergence": False}}

problem = fe.NonlinearVariationalProblem(F, X, BC_u, H)

solver = fe.NonlinearVariationalSolver(problem)
solver.parameters.update(snes_solver_parameters)
solver.solve()

uu, dd = X.split()

fe.plot(uu)
plt.xlabel("x")
plt.ylabel("u(x)")
plt.figure()
fe.plot(dd)
plt.xlabel("x")
plt.ylabel("d(x)")
plt.show()

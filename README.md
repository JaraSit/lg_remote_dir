# LG remote directory

## Content

LG directory
* lg_beams_solver.py - elastic solver for laminated glass beams
* pf_beams_v1.py - phase-field solver for 1L beams
* 1D_Pham.py - One-dimensional testing of Pham version of Phase-field*

FEniCS_tests directory
* mindlin_beam.py - basic Mindlin beam solver
* mindlin_beam_shearlock.py - testing of shear-locking prevention
* plate.py - solver for Reissner-Mindlin plates
* plate_shearlock.py - testing of shear-locking prevention for plates
* newmark_3d_test.py - Newmark viscoelastic solver for 3D cube example (not working)

\* old code, not controlled

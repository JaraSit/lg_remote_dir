from fenics import*
import matplotlib.pyplot as plt


# TODO: Temporary. Todo automated assembly!!
def map_to_layers(fce, h):
    u = [None]*3
    w = [None]*3
    phi = [None]*3
    gam = [None]*3

    u[0] = fce[1]
    u[2] = fce[3]
    w[0] = fce[0]
    w[1] = fce[0]
    w[2] = fce[0]
    phi[0] = fce[2]
    phi[2] = fce[4]
    u[1] = 0.25*h[0]*phi[0] - 0.25*h[2]*phi[2] + u[2]
    phi[1] = -0.5*phi[0]*h[0]/h[1]-0.5*phi[2]*h[2]/h[1]+u[2]/h[1]-u[0]/h[1]
    gam[0] = fce[5]
    gam[1] = fce[6]
    gam[2] = fce[7]

    return u, w, phi, gam


def beam_solver():
    # --------------------
    # Parameters
    # --------------------
    # number of layers
    n_l = 3
    # number of glasses
    n_glass = 2

    E = [64.5e9, 2.8e6, 64.5e9]
    G = [26.2e9, 1.0e6, 26.2e9]
    b = 0.05
    h = [0.00212, 0.00076, 0.00212]
    Ar = [b*hi for hi in h]
    I = [1.0/12.0*b*hi**3 for hi in h]

    k = 5.0/6.0
    n = 100
    l = 1.5
    F = 1.0

    # number of unknowns
    n_unk = 2*n_glass + 1 + n_l

    # --------------------
    # Define geometry
    # --------------------
    mesh = IntervalMesh(n, 0, l)

    # --------------------
    # Define spaces
    # --------------------
    P1 = FiniteElement('P', interval, 1)
    P2 = FiniteElement('DG', interval, 0)
    element = MixedElement(n_unk*[P1])
    V = FunctionSpace(mesh, element)

    # Unknowns = w1, u1, phi1, u3, phi3, u5, phi5, ...
    d_u = TestFunctions(V)
    u_tr = TrialFunctions(V)

    # --------------------
    # Boundary conditions
    # --------------------
    bc_u = Constant(0.0)

    def left_end(x):
        return near(x[0], 0.0)

    def right_end(x):
        return near(x[0], l)

    def all_bounds(x, on_boundary):
        return on_boundary

    bc1 = DirichletBC(V.sub(0), bc_u, all_bounds)
    bc2 = DirichletBC(V.sub(1), bc_u, all_bounds)
    bc3 = DirichletBC(V.sub(2), bc_u, all_bounds)
    bc4 = DirichletBC(V.sub(3), bc_u, all_bounds)
    bc5 = DirichletBC(V.sub(4), bc_u, all_bounds)
    bc = [bc1, bc2, bc3, bc4, bc5]

    # --------------------
    # Initialization
    # --------------------
    X = Function(V)

    # Remapping to layers
    u, w, phi, gam = map_to_layers(u_tr, h)
    du, dw, dphi, dgam = map_to_layers(d_u, h)

    # --------------------
    # Variational form
    # --------------------
    A = sum(du[i].dx(0)*E[i]*Ar[i]*u[i].dx(0)*dx for i in range(0, n_l))
    A += sum(dw[i].dx(0)*k*G[i]*Ar[i]*gam[i]*dx for i in range(0, n_l))
    A += sum(dphi[i].dx(0)*E[i]*I[i]*phi[i].dx(0)*dx + dphi[i]*k*G[i]*Ar[i]*gam[i]*dx for i in range(0, n_l))
    # Penalty terms for shear-locking
    A += sum(dgam[i]*k*G[i]*Ar[i]*(gam[i] - w[i].dx(0) - phi[i])*dx for i in range(0, n_l))
    A += Constant(0.0)*dw[0]*dx

    # --------------------
    # Solution
    # --------------------
    A_ass, b_ass = assemble_system(lhs(A), rhs(A), bc)

    point_force = PointSource(V.sub(0), Point(0.75), 15)
    point_force.apply(b_ass)

    solve(A_ass, X.vector(), b_ass)

    u_, w_, phi_, gam_ = map_to_layers(X.split(deepcopy=True), h)

    plot(w_[0])
    plt.show()

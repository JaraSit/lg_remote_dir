
# -------------------------
# Impact response of steel impactor
# -------------------------

# -------------------------
# Description:
#
# Last edit: 28.03. 2019
# -------------------------

import fenics as fe
import mshr as ms
import matplotlib.pyplot as plt
import math


# --------------------
# Functions and classes
# --------------------
def eps(v):
	return fe.sym(fe.grad(v))


def sigma(v):
	dim = v.geometric_dimension()
	return 2.0*mu*eps(v) + lmbda*fe.tr(eps(v))*fe.Identity(dim)


def get_force(a):
	if a <= t_c:
		return math.sin(a*math.pi/t_c)
	else:
		return 0.0


# --------------------
# Geometry
# --------------------
box1 = ms.Box(fe.Point(0, 0, 0), fe.Point(0.03, 0.4, 0.12))
box2 = ms.Box(fe.Point(0.03, 0.175, 0), fe.Point(0.798, 0.225, 0.12))
box3 = ms.Box(fe.Point(0.798, 0.14, 0), fe.Point(0.823, 0.26, 0.12))
sphere = ms.Sphere(fe.Point(0.823, 0.2, 0.06), 0.05)
boxD1 = ms.Box(fe.Point(0.773, 0.14, 0), fe.Point(0.798, 0.175, 0.12))
boxD2 = ms.Box(fe.Point(0.773, 0.225, 0), fe.Point(0.798, 0.26, 0.12))

domain = box1 + box2 + box3 + sphere - boxD1 - boxD2

# Create mesh
mesh = ms.generate_mesh(domain, 50)
fe.plot(mesh)
plt.show()

# --------------------
# Parameters
# --------------------
E, nu = 210.0e9, 0.3
rho = 7850.0

# Time parameters
dt = 2.0e-4  # time increment
t = 0.0  # start time
t_end = 1.0e-2  # end time
t_c = 3.0e-3  # Impulse duration

# Lame coefficient for constitutive relation
mu = E/2.0/(1.0+nu)
lmbda = E*nu/(1.0+nu)/(1.0-2.0*nu)

# --------------------
# Define spaces
# --------------------
V = fe.VectorFunctionSpace(mesh, "CG", 2)
u_tr = fe.TrialFunction(V)
u_test = fe.TestFunction(V)

# --------------------
# Initialization
# --------------------
u = fe.Function(V, name="Displacement")
u_bar = fe.Function(V)
du = fe.Function(V)
ddu = fe.Function(V)
ddu_old = fe.Function(V)

xdmffile_u = fe.XDMFFile('Solution/u.xdmf')

# --------------------
# Variational form
# --------------------
A = fe.inner(sigma(u_tr), eps(u_test))*fe.dx + 4*rho/(dt*dt)*fe.dot(u_tr - u_bar, u_test)*fe.dx

# --------------------
# Time integration
# --------------------
while t < t_end:
	u_bar.assign(u + dt*du + 0.25*dt*dt*ddu)

	K, b = fe.assemble_system(fe.lhs(A), fe.rhs(A), [])

	F = get_force(t)
	print("External force: ", F)
	p = fe.PointSource(V.sub(0), fe.Point(0.823, 0.2, 0.06), -F)
	p.apply(b)

	fe.solve(K, u.vector(), b)

	xdmffile_u.write(u, t)

	ddu_old.assign(ddu)
	ddu.assign(4/(dt*dt)*(u-u_bar))
	du.assign(du + 0.5*dt*(ddu + ddu_old))

	print("Time: ", t)
	t = t+dt

xdmffile_u.close()

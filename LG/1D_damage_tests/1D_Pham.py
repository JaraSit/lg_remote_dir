from fenics import *
import matplotlib.pyplot as plt

# --------------------
# Parameters
# --------------------
n = 200
l = 1.0
time_start = 0.0
time_end = 0.35
A = 1.0
tau = 2.0e-3
lc = 0.01
Gc = 0.1

# --------------------
# Define geometry
# --------------------
mesh = IntervalMesh(n, 0.0, l)

plot(mesh, "Mesh")
plt.show()

# --------------------
# Define spaces
# --------------------
V = FunctionSpace(mesh, 'CG', 1)
P1 = FiniteElement('P', interval, 1)
element = MixedElement([P1, P1])
W = FunctionSpace(mesh, element)
s = Function(V)
u = TrialFunction(V)
v = TestFunction(V)

# --------------------
# Boundary conditions
# --------------------
tol = 1.0e-14


def left_end(x):
	return near(x[0], 0.0)


def right_end(x):
	return near(x[0], l)


def middle_point(x):
	return near(x[0], l / 2)


u_D = Expression("t", t=0.0, degree=0)
BC_u_1 = DirichletBC(V, Constant(0.0), left_end)
BC_u_2 = DirichletBC(V, u_D, right_end)
BC_u = [BC_u_1, BC_u_2]
BC_s_1 = DirichletBC(V, 0.1, middle_point)
BC_s = [BC_s_1]

BC_nl_u_1 = DirichletBC(W.sub(0), Constant(0.0), left_end)
BC_nl_u_2 = DirichletBC(W.sub(0), 0.1, right_end)
BC_nl_s_1 = DirichletBC(W.sub(1), 0.0, left_end)
BC_nl_s_2 = DirichletBC(W.sub(1), 0.0, right_end)
BC_nl_s_3 = DirichletBC(W.sub(1), 0.8, middle_point)
BC_nl = [BC_nl_u_1, BC_nl_u_2]

# --------------------
# Initialization
# --------------------
u_ = Function(V)
u_old = Function(V)
s_ = Function(V)
s_old = Function(V)
s_2 = Function(V)
H = Function(V)

lower = Function(V)
upper = Function(V)
lower.interpolate(Constant(0.0))
upper.interpolate(Constant(1.0))

E = Expression("1.0 - 0.1*near(x[0], 0.5, 0.001)", degree=1)

# --------------------
# Staggered loop
# --------------------
iterr = 1
err = 1
toll = 1.0e-4
maxiter = 10

t = time_start + tau
time = []
stress = []

while t <= time_end:

	u_D.t = t

	while True:

		# Variational problem
		E_du = (1 - s) ** 2 * E * A * inner(grad(u), grad(v)) * dx

		# Solution
		solve(lhs(E_du) == rhs(E_du), u_, BC_u)

		# Pham formulation
		E_ds = 0.5 * E * A * inner(grad(u_), grad(u_)) * inner((1.0 - s), (1.0 - s)) * dx + Gc * (
				s + 0.5 * lc * lc * inner(grad(s), grad(s))) * dx

		# Solution of damage formulation
		F = derivative(E_ds, s, TestFunction(V))
		H = derivative(F, s, TrialFunction(V))

		snes_solver_parameters = {"nonlinear_solver": "snes",
									"snes_solver": {"linear_solver": "lu",
									"relative_tolerance": 1e-4,
									"maximum_iterations": 20,
									"report": True,
									"error_on_nonconvergence": False}}

		problem = NonlinearVariationalProblem(F, s, [], H)
		problem.set_bounds(lower, upper)

		solver = NonlinearVariationalSolver(problem)
		solver.parameters.update(snes_solver_parameters)
		solver.solve()

		# Errors
		if norm(u_) == 0:
			err_u = 1.0
		else:
			err_u = norm(project(u_ - u_old, V)) / norm(u_)
		if norm(s) == 0:
			err_s = 1.0
		else:
			err_s = norm(project(s - s_old, V)) / norm(s)
		err = max(err_u, err_s)
		print('iter', iterr, 'error', err)

		# Assignment
		u_old.assign(u_)
		s_old.assign(s)

		# Increment
		iterr = iterr + 1

		# interruption condition
		if (err < toll) or (iterr > maxiter):
			print('Solution converges after: ', iterr)
			break

	time.append(t)
	sigma = project((1.0 - s) ** 2 * E * u_.dx(0), V)
	stress.append(sigma.vector().get_local()[50])

	t = t + tau

plot(sigma, title="stress")
plt.show()
plot(u_)
plt.show()
plot(s)
plt.show()

plt.plot(time, stress, 'r')
plt.show()

from scipy.integrate import odeint
import matplotlib.pyplot as plt
import numpy as np
import math

# -------------------------
# Reference solution for 3dof model
# -------------------------

# -------------------------
# Description:
# dof3_contact = solution with Hertz force P = alpha*delta_u
# dof3_contact_nlin = solution with Hertz force P = alpha*(delta_u)^(3/2)
# Last edit: 01.02. 2019
# -------------------------


# Macauley bracket
def mc_bracket(x):
	return 0.5*(x+abs(x))


# Definition of problem for ODE solver
# Linear Hertz, nonlinear tension-compression
def dof3_contact(y, t):
	r1, r2, r0, v1, v2, v0 = y
	dydt = [v1, v2, v0, -1.0/m1*(k*r1-k*r2), -1.0/m2*(-k*r1+k*r2+alpha*mc_bracket(r2-r0)), -1.0/m0*(-alpha*mc_bracket(r2-r0))]
	return dydt


# Nonlinear Hertz, nonlinear tension-compression
def dof3_contact_nlin(y, t):
	r1, r2, r0, v1, v2, v0 = y
	dydt = [v1, v2, v0, -1.0/m1*(k*r1-k*r2), -1.0/m2*(-k*r1+k*r2+alpha*mc_bracket(r2-r0)**1.5), -1.0/m0*(-alpha*mc_bracket(r2-r0)**1.5)]
	return dydt


# Beam parameters
l = 1.5
h = 0.01
rho = 2500
E1 = 70e9
E2 = 210e9
nu1 = 0.23
nu2 = 0.3
I = 1.0/12.0*l*h**3
k = 48.0*E1*I/(l**3)
m = l*l*h*rho
m1 = m*0.107
m2 = m*0.893
m0 = 50.0
h0 = 0.2  # initial height of impactor
v0 = -math.sqrt(2*10*h0)  # initial velocity of impactor
R = 0.05
alpha = 4.0/3.0*math.sqrt(R)/((1.0-nu1**2)/E1+(1.0-nu2**2)/E2)

# Initial values
r0v0 = [0, 0, 0, 0, 0, v0]

# Time discretization
t = np.linspace(0.0, 1.0e-1, 1000)
# Solution
sol = odeint(dof3_contact_nlin, r0v0, t, args=())


# Post-process: ploting
plt.subplot(2, 1, 1)
plt.title("Displacements")
plt.plot(t, sol[:, 0], '-', color='blue', label=r'r1')
plt.plot(t, sol[:, 1], '-', color='red', label=r'r2')
plt.plot(t, sol[:, 2], '-', color='gray', label=r'r0')
plt.legend()

plt.subplot(2, 1, 2)
plt.title("Velocities")
plt.plot(t, sol[:, 3], '-', color='blue', label=r'v1')
plt.plot(t, sol[:, 4], '-', color='red', label=r'v2')
plt.plot(t, sol[:, 5], '-', color='gray', label=r'v0')
plt.legend()
plt.show()